package eu.man.c4s.copycode.config;

import java.util.Collections;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Value("${info.app.version}")
    private String appVersion;

    @Bean
    public Docket productApi() {
        return new Docket(DocumentationType.SWAGGER_2)
            .apiInfo(metaData())
            .select()
            .apis(RequestHandlerSelectors.any())
            .paths(PathSelectors.any())
            .build();
    }

    private ApiInfo metaData() {
        ApiInfo apiInfo = new ApiInfo(
            "C4S Copy Code",
            "Copy code service",
            appVersion,
            "http://www.man.eu",
            new Contact("C4S", "http://www.man.eu", "@man.eu"),
            "License to MAN Bus & Truck AG Munich",
            "http://www.man.eu",
            Collections.emptyList()
        );
        return apiInfo;
    }
}
