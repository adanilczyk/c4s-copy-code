package eu.man.c4s.copycode.resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Resource {

    @GetMapping("/hello/{name}")
    public String halloWorld(@PathVariable String name){
        return "hallo " + name;
    }
}
