package eu.man.c4s.copycode;

import eu.man.c4s.code.entity.ManCodeResponse;
import eu.man.c4s.code.service.ManCodeService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.scheduling.annotation.EnableScheduling;

import javax.net.ssl.*;

@EnableScheduling
@SpringBootApplication(scanBasePackages = {"eu.man.*"})
@Log4j2
public class CopyCodeApp extends SpringBootServletInitializer implements CommandLineRunner {

    @Autowired
    private ManCodeService codeService;

    public static void main(String[] args) {
        SpringApplication.run(CopyCodeApp.class, args);
    }


    @Override
    public void run(String... strings) throws Exception {

        try {
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, doTrustToCertificates(), new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

            
            ManCodeResponse codeResponse = codeService.load("EXA58DNS");

        } catch (Exception e) {
            log.error("not possible to load all trusted certificates");
            e.printStackTrace();
        }
    }

    public TrustManager[] doTrustToCertificates() throws Exception {


        TrustManager[] trustAllCerts = new TrustManager[]{
            new X509TrustManager() {

                public java.security.cert.X509Certificate[] getAcceptedIssuers()
                {
                    return null;
                }
                public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType)
                {
                    //No need to implement.
                }
                public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType)
                {
                    //No need to implement.
                }
            }
        };
        return trustAllCerts;
    }

}
